package nl.utwente.di.Converter;

public class Calculator {

    double getTemperature(double fahrenheit) {
        double celsius = ( (fahrenheit - 32) * 5) / 9;
        return celsius;
    }
}
