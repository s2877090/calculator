package nl.utwente.di.Converter;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** This is the calculator for the fahrenhet to degrees calculator
 */

public class Converter extends HttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Calculator calculator;

    public void init() throws ServletException {
        calculator = new Calculator();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Calculator";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Fahrenheit: " +
                request.getParameter("isbn") + "\n" +
                "  <P>Celsius: " +
                Double.toString(calculator.getTemperature(Double.parseDouble(request.getParameter("isbn")))) +
                "</BODY></HTML>");
    }


}
